CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c11 -I..
LDFLAGS=-lm

all: teste_aluno teste_professor
#teste_professor

arvore.o: arvore.c
main.o: main.c
test.o: test.c
le.o: le.c

# coloque outras dependencias aqui

main: arvore.o le.o main.o $(LDFLAGS)
test: arvore.o le.o test.o $(LDFLAGS)


teste_aluno: main
	./main

teste_professor: test
	./test

clean:
	rm -f *.o test aluno

