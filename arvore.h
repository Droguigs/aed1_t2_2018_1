#ifndef _ARVORE_H_
#define _ARVORE_H_

#include "le.h"
/* IMPLEMENTAÇÃO DE ÁRVORE BINÁRIA DE PESQUISA */

struct nodo {
	int valor;
	struct nodo * dir, * esq;
};

/* Inicializa uma árvore
 * @param n número de valores iniciais
 * @param elems vetor com valores a serem inseridos
 * @return raiz da árvore ou NULL se erro
 */ 
struct nodo * inicializa_arvore(int n, int * elems);

/* Insere nodo em uma árvore se a chave ainda não existir, sempre como uma folha
 * @param raiz raiz da árvore
 * @param valor o que será inserido
 * @return raiz da árvore
 */ 
struct nodo * insere_nodo(struct nodo * raiz, int elem);

/* Remove nodo em uma árvore se existir, dando prioridade à subárvore esquerda para novo nodo raiz
 * @param raiz raiz da árvore
 * @param valor o que será removido
 * @return raiz da árvore
 */ 
struct nodo * remove_nodo(struct nodo * raiz, int elem);

/* Altura de uma árvore
 * @param raiz raiz da árvore
 * @return altura da árvore
 */
int altura(struct nodo * raiz);

/* Consulta se nodo existe
 * @param node raiz da árvore
 * @param valor chave a ser buscada
 * @return ponteiro para nodo, ou NULL se inexistente
 */
struct nodo * busca(struct nodo * node, int elem);

/* Descobre se uma árvore está balanceada
 * @param raiz da árvore
 * @return diferença das alturas das subárvores
 */
int balanceada(struct nodo * head);


/* Número de elementos em uma árvore 
 * @param head raiz da árvore
 * @return número de elementos da árvore 
 */
int numero_elementos(struct nodo * head);

/* Percorre a árvore em abrangência 
 * @param raiz raiz da árvore
 * @param resultado vetor onde será armazenado o percurso (já deve estar alocado)
 * @return número atual de elementos no vetor
 */
int abrangencia(struct nodo * raiz, int * resultado);

/* Percorre a árvore de forma pré-fixada 
 * @param head raiz da árvore
 * @param resultado vetor onde será armazenado o percurso (já deve estar alocado)
 * @return número atual de elementos no vetor
 */
int prefix(struct nodo * head, int * result);

/* Percorre a árvore de forma pós-fixada 
 * @param head raiz da árvore
 * @param resultado vetor onde será armazenado o percurso (já deve estar alocado)
 * @return número atual de elementos no vetor
 */
int postfix(struct nodo * head, int * result);

/* Percorre a árvore de forma infix 
 * @param head raiz da árvore
 * @param resultado vetor onde será armazenado o percurso (já deve estar alocado)
 * @return número atual de elementos no vetor
 */
int infix(struct nodo * head, int * result);

/* Imprime na stdio os valores de um caminhamento com um espaço entre cada valor,
 * máximo de 10 valores por linha
 * @param valores vetor com valores a serem impressos
 * @param tamanho número de entradas no vetor de valores
 */
void imprime(int * valores, int tamanho);

/* Remove todos os nodos
 * @param raiz da árvore
 * @return raiz (null)
 */
struct nodo* remove_todos(struct nodo * raiz);

/* Acha o menor filho a esquerdo da raiz
 * @param node raiz da arvore
 * @return nova raiz
 * */
struct nodo* inorderSuccessor(struct nodo* node);

/*Acha o maior entre 2 inteiros
 *@param a primeiro inteiro
 *@param b segundo inteiro
 *@return maior entre a e b
 * */
int max(int a, int b);

/*Coloca o ponteiro a ser percorrido na fila de exe.
 * @param head ponteiro com o valor
 * @param list fila a ser percorrida
 * */
void enQueue(struct nodo **head, struct llist *list);

/*Pega o valor armazenado na primeira posicao da fila
 * @param list fila onde estão os valores
 * @return valor armazenado
 * */
int deQueue(struct llist *list);

/*Remove o valor armazenado na primeira posicao da fila
 * @param list fila onde estão os valores
 * @return valor armazenado
 * */
void percorre(struct llist *list);

#endif 
