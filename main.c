//
// Created by schia on 29/05/2018.
//

#include "arvore.h"
#include <stdlib.h>
#include <stdio.h>

struct nodo* createTree(int *elems){
  return inicializa_arvore(15, elems);
}

void testTree(struct nodo* head){
  if (head==NULL){
    printf("\n\n*~Arvore Vazia~*\n");
    return;
  }else {
    int j = numero_elementos(head), jj = 0;
    int i[j];

    printf("\n\nInfix: ");
    jj = infix(head, i);
    imprime(i, jj);

    printf("\nPrefix: ");
    jj = prefix(head, i);
    imprime(i, jj);

    printf("\nPostfix: ");
    jj = postfix(head, i);
    imprime(i, jj);
  }
}

int main(){
  int elems[] = {7, 3, 1, 5, 0, 2, 4, 6, 11, 9, 8, 10, 13, 12, 14};
  int elemsLista[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14};

  struct nodo *head;

  head = createTree(elemsLista);

  testTree(head);
  printf("\n%d - Balanceamento ", balanceada(head));
  head = remove_todos(head);
  testTree(head);

  head = createTree(elems);

  balanceada(head);
  testTree(head);
  printf("\n%d - Balanceamento ", balanceada(head));

  printf("\n\nRemove 14");
  head = remove_nodo(head, 14);
  testTree(head);
  printf("\n%d - Balanceamento ", balanceada(head));

  printf("\n\nRemove 12");
  head = remove_nodo(head, 12);
  testTree(head);
  printf("\n%d - Balanceamento ", balanceada(head));

  printf("\n\nRemove 7");
  head = remove_nodo(head, 7);
  testTree(head);
  printf("\n%d - Balanceamento ", balanceada(head));

  printf("\n\nInsere 14");
  insere_nodo(head, 14);
  testTree(head);
  printf("\n%d - Balanceamento ", balanceada(head));

  printf("\n\nInsere 17");
  insere_nodo(head, 17);
  testTree(head);
  printf("\n%d - Balanceamento ", balanceada(head));

  printf("\n\nInsere 15");
  insere_nodo(head, 15);
  testTree(head);
  printf("\n%d - Balanceamento ", balanceada(head));

  head = remove_todos(head);
  testTree(head);

  return 0;
}
